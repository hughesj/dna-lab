

//****************************
// Honor Code: This work is mine unless otherwise cited.
// John Hughes and Jerry He
// CMPSC 111 Fall 2015
// Lab #5
// Date: September 24, 2015
//
// Purpose: DNA Manipulation
//****************************
import java.util.Date;
import java.util.Scanner;
import java.util.Random;

public class DnaManipulation
{
    //---------------------
    // main method: program execution begins here
    //---------------------
    public static void main(String[] args)
    {
        // Label output with name and date:
        System.out.println("John Hughes and Jerry He\n Lab #5\n" + new Date() + "\n");

        // variable dictionary
        Scanner scan = new Scanner(System.in);
        Random r = new Random();
        String dnaString, complement, mutation, deletion, replacement, teletion;
        int len, location, del, tel, pel;
        char c, d, e, q;

        // ask user for dna string and store it in dnaString variable
        System.out.println("Input a DNA string using only C, G, T, and A: ");
        dnaString = scan.nextLine();

        // find compliment of strand
        dnaString = dnaString.toLowerCase();
        System.out.println("Lowercase String: "+dnaString);
        dnaString = dnaString.toUpperCase();
        System.out.println("Uppercase String: "+dnaString);
        complement = dnaString.replace('A','a');
        complement = complement.replace('C','c');
        complement = complement.replace('T','A');
        complement = complement.replace('a','T');
        complement = complement.replace('G','C');
        complement = complement.replace('c','G');

        System.out.println("Your converted string is: "+complement);

        //mutation by inserting letter
        System.out.println("\nWe will now mutate your string at a random place");
        len = dnaString.length();
        System.out.println("The length of your string " +dnaString+"="+len);
        location = r.nextInt(len+1);
        System.out.println("Here's everything up to location "+ location +": " +dnaString.substring(0,location));
        System.out.println("and the rest from location: "+location +" so forth: "+ dnaString.substring(location));
        c = dnaString.charAt(r.nextInt(len));
        mutation = dnaString.substring(0,location) +c+ dnaString.substring(location);
        System.out.println("The new letter is: "+c);
        System.out.println("We will put this new letter in location "+location+" in "+ dnaString);
        System.out.println("Thus your new mutation is: "+mutation);

        //mutation by deleting a letter
        System.out.println("We will now mutate the string by deleting a letter from the original string");
        del = r.nextInt(len); //not +1 since we can't delete after the last letter
        d = dnaString.charAt(del);
        deletion = dnaString.substring(0,del) +""+ dnaString.substring(del+1);
        System.out.println("Deleting letter "+d+" at location "+del+" gives us "+deletion);

        //mutation by replacing a letter
        System.out.println("We will now alter a single letter from a randomly-chosen position");
        tel = r.nextInt(len);
        e = dnaString.charAt(tel);
        pel = r.nextInt(len);
        q = dnaString.charAt(pel);
        e = q;
        teletion = dnaString.substring(0,tel)+ e +dnaString.substring(tel+1);
        System.out.println("Inserting a "+q+" at location "+tel+" gives us "+teletion);

    }
}
